package com.jcons.pdfconverter.pdfconverter;

public abstract class Base {
    public abstract void clear();
    public abstract String toPDFString();
}
