package com.jcons.pdfconverter.pdfconverter;


public class Stream extends EnclosedContent {

    public Stream() {
        super("stream\n", "endstream\n");
    }

}
